# Warhouse Test Developer Task

## Description
Source code and issue tracking for developer candidate task. 

## Usage
Primary method of use will be via the [web interface](http://localhost:8000/)

## Brief
### Overview
In order to assess your technical and problem solving skills we ask development candidates
to complete a small sample application.

The application is based on a fictional warehouse laid out as below. The warehouse contains
60 product locations and three picking stations. When an order is picked, warehouse staff
must visit each of the bin locations which contains a product on the order, take the required
quantity and transport all of the items to a picking station.

|   |     | Y   |   |     |     |   |     |     |   |
|---|-----|-----|---|-----|-----|---|-----|-----|---|
|   | A10 | B10 |   | C10 | D10 |   | E10 | F10 |   |
|   | A9  | B9  |   | C9  | D9  |   | E9  | F9  |   |
|   | A8  | B8  |   | C8  | D8  |   | E8  | F8  |   |
|   | A7  | B7  |   | C7  | D7  |   | E7  | F7  |   |
|   | A6  | B6  | X | C6  | D6  |   | E6  | F6  |   |
|   | A5  | B5  |   | C5  | D5  |   | E5  | F5  |   |
|   | A4  | B4  |   | C4  | D4  |   | E4  | F4  |   |
|   | A3  | B3  |   | C3  | D3  |   | E3  | F3  |   |
|   | A2  | B2  |   | C2  | D2  |   | E2  | F2  |   |
|   | A1  | B1  |   | C1  | D1  |   | E1  | F1  |   |
|   | P1  |     |   | P2  |     |   | P3  |     |   |

### Notes
- Products exist in bin locations from A1-F10
- Picking stations are marked as P1, P2 and P3
- A picker can only pick a bin from the side
  -  A picker standing at X can pick from bins B6 and C6
  -  A picker standing at Y cannot pick from anywhere
- A picker can walk directly through the packing stations but cannot walk through any
product bins
### Requirements
1. Create a database of 60 products. Assign each a stock level and a unique bin
location from the range A1 - F10 (see the warehouse floor plan below).
2. Provide a tool for querying a product or a bin location. It should return the product
description, bin location and current stock level.
3. Provide a tool to generate a picking route for a list of five or more products. The
algorithm should consider factors such as speed, efficiency and scalability.
4. All routes should start and end at a picking station. Your algorithm may choose which
ones.
5. The solution must be written in PHP and demonstrate an object oriented approach.
6. The database should be MySQL.
7. Third party libraries are allowed for providing basic functionality however the route
algorithm itself must be your own code.
8. Provide a brief explanation of the approach taken and any considerations that went
into it. Discuss how your solution would scale to a larger warehouse or orders with
more products.
9. Provide instructions on how to use the tools you have written.