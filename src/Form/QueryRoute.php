<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class QueryRoute extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item1Id', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g. 24',
                ],
                'label' => 'Item 1 Product Id',
                'required' => true,
            ])
            ->add('item2Id', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g. 17',
                ],
                'label' => 'Item 2 Product Id',
                'required' => true,
            ])
            ->add('item3Id', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g. 53',
                ],
                'label' => 'Item 3 Product Id',
                'required' => true,
            ])
            ->add('item4Id', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g. 60',
                ],
                'label' => 'Item 4 Product Id',
                'required' => true,
            ])
            ->add('item5Id', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g. 9',
                ],
                'label' => 'Item 5 Product Id',
                'required' => true,
            ])
            ->getForm();
    }
}
