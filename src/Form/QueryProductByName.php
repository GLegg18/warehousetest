<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class QueryProductByName extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'e.g. PlayStation 5 (PS5)',
                ],
                'help' => 'Currently requires exact string match',
                'label' => 'By Product Name',
                'required' => false,
                'empty_data' => 'PlayStation 5 (PS5)',
            ])
            ->getForm();
    }
}
