<?php

namespace App\Repository;

use App\Entity\Product;

interface ProductRepositoryInterface
{
    public function add(Product $investment): void;

    public function remove(Product $investment): void;

    public function findById(int $investment): ?Product;

    public function size(): int;
}
