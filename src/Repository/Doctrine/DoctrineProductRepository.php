<?php

namespace App\Repository\Doctrine;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

final class DoctrineProductRepository implements ProductRepositoryInterface
{
    private LoggerInterface $logger;
    private EntityManagerInterface $entityManager;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    public function add(Product $Product): void
    {
        $this->entityManager->persist($Product);
    }

    public function remove(Product $Product): void
    {
        $this->entityManager->remove($Product);
    }

    public function findById(int $ProductId): ?Product
    {
        return $this->entityManager->find(Product::class, $ProductId);
    }

    public function size(): int
    {
        return $this->entityManager->createQueryBuilder()
            ->select('count(Product.id)')
            ->from(Product::class, 'Product')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findProductById(int $filters): array
    {
        $conn = $this->entityManager->getConnection();
        $qb = $conn->createQueryBuilder();

        $qb
            ->select(
                'id',
                'name',
                'stock_level',
                'bin_location',
                'description'
            )
            ->from('product', 'p')
            ->where('p.id=:id');

        $qb
            ->setParameter('id', $filters);

        $stmt = $qb->execute();
        return $stmt->fetchAllAssociative();
    }

    public function findProductByName(string $filters): array
    {
        $conn = $this->entityManager->getConnection();
        $qb = $conn->createQueryBuilder();

        $qb
            ->select(
                'id',
                'name',
                'stock_level',
                'bin_location',
                'description'
            )
            ->from('product', 'p')
            ->where('p.name=:name');

        $qb
            ->setParameter('name', $filters);


        $stmt = $qb->execute();
        return $stmt->fetchAllAssociative();
    }
}
