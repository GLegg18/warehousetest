<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @package App\Controller
 */
class BaseController extends AbstractController
{
    private $logger;

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="warehouseHome")
     */
    public function warehouseHome(): Response
    {
        $this->logger->debug('warehouseHome Page');

        return $this->render('index.html.twig', []);
    }
}
