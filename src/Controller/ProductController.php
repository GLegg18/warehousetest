<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Form\QueryProductById;
use App\Form\QueryRoute;
use App\Repository\Doctrine\DoctrineProductRepository;
use App\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 * @package App\Controller
 */
class ProductController extends AbstractController
{
    private $logger;
    private $DoctrineProductRepository;


    public function __construct(
        LoggerInterface $logger,
        DoctrineProductRepository $DoctrineProductRepository
    ) {
        $this->logger = $logger;
        $this->DoctrineProductRepository = $DoctrineProductRepository;
    }

    /**
     * @Route("/product/{id}", name="product_show")
     */
    public function show(int $id, ProductRepositoryInterface $ProductRepositoryInterface): Response
    {
        // swap to sql query builder querying for custom queries
        $product = $ProductRepositoryInterface
            ->findById($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        // obviously replace eventually with a template
        return new Response('Check out this great product: ' . $product->getName());
    }

    /**
     * @Route("/query/product", name="queryProduct")
     */
    public function queryProduct(Request $request): Response
    {
        $this->logger->info('In queryProductById page');
        $filter = array(
            'id' => 1,
        );
        $form = $this->createForm(QueryProductById::class, $filter);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filter = $form->getData();
        }

        $this->logger->info('filter: ', $filter);
        // $filterParams = [$filters['id'], $filters['name']];
        $data = $this->DoctrineProductRepository->findProductById($filter['id']);
        return $this->render('product/queryProduct.html.twig', [
            'product' => $data,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/route-picker", name="routePicker")
     */
    public function routPicker(Request $request): Response
    {
        $this->logger->info('In routePicker page');
        $filters = array(
            'item1Id' => 16,
            'item2Id' => 41,
            'item3Id' => 22,
            'item4Id' => 8,
            'item5Id' => 28,
        );
        $form = $this->createForm(QueryRoute::class, $filters);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filters = $form->getData();
        }

        // add search logic to get bins of each item
        $bins = [];
        foreach ($filters as $id) {
            $product = $this->DoctrineProductRepository->findProductById($id);
            $location = $product[0]['bin_location'];
            array_push($bins, $location);
        }

        // turn into associative array made from col letter and number
        $associativeBins = [];
        foreach ($bins as $bin){
            // get first character of string
            $letter = $bin[0];
            // get number at end of string
            $num = substr($bin,1);
            $associativeBins[$bin] = array(
                'col' => $letter,
                'num' => $num,
            );
        };

        // order bins alphabetically and numerically based on associative array
        asort($associativeBins);
        $orderedBins = [];
        foreach ($associativeBins as $key => $val) {
            array_push($orderedBins, $key);
        }
        $this->logger->info('ORDEREDBINS: ', $orderedBins);

        // pick start station
        $firstBin = $orderedBins[0];
        $binLetter = $firstBin[0];
        if ($binLetter == 'A'){
            $startStation = 'P1';
        } if ($binLetter == 'B' || $binLetter == 'C') {
            $startStation = 'P2';
        } elseif ($binLetter == 'D' || $binLetter == 'E' || $binLetter == 'F') {
            $startStation = 'P3';
        };
        
        // pick end station
        $lastBin = end($orderedBins);
        $binLetter = $lastBin[0];
        if ($binLetter == 'A'){
            $endStation = 'P1';
        } if ($binLetter == 'B' || $binLetter == 'C') {
            $endStation = 'P2';
        } elseif ($binLetter == 'D' || $binLetter == 'E' || $binLetter == 'F') {
            $endStation = 'P3';
        };

        return $this->render('routePicker/routePicker.html.twig', [
            'form' => $form->createView(),
            'orderedBins' => $orderedBins,
            'startStation' => $startStation,
            'endStation' => $endStation,
        ]);
    }

}
